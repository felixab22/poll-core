package com.sistemas.poll.dto;

import java.util.List;

/**
 * Created by AITAMH on 8/01/2020.
 */
public class ReporteEncuestaDto {

    private Object ponderacion;
    private Object calificacion;

    public Object getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(Object ponderacion) {
        this.ponderacion = ponderacion;
    }

    public Object getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Object calificacion) {
        this.calificacion = calificacion;
    }

    @Override
    public String toString() {
        return "ReporteEncuestaDto{" +
                "ponderacion=" + ponderacion +
                ", calificacion=" + calificacion +
                '}';
    }
}
