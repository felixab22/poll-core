package com.sistemas.poll.payload;

import com.sistemas.poll.model.Persona;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by Aitamh on 19/08/17.
 */
public class JwtAuthenticationResponse {

    private String accessToken;
    private String tokenType = "Bearer";
    private String username;
    private Collection<? extends GrantedAuthority> authorities;

    private Persona persona;

    public JwtAuthenticationResponse(String accessToken, String username, Collection<? extends GrantedAuthority> authorities, Persona persona) {
        this.accessToken = accessToken;
        this.username = username;
        this.authorities = authorities;
        this.persona = persona;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }


}
