package com.sistemas.poll.dao;

import com.sistemas.poll.model.Cargaacademica;
import com.sistemas.poll.model.Pregunta;
import com.sistemas.poll.model.Respuesta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by AITAMH on 8/01/2020.
 */
public interface IRespuestaDao extends JpaRepository<Respuesta, Long> {

    @Query("SELECT SUM (r.ponderacion), r.descripcion FROM Respuesta r WHERE r.cargaacademica = :idcarga AND r.pregunta = :idpregunta GROUP BY r.descripcion")
    List<String> reportConsolidadoEncuestaByPregunta(@Param("idcarga") Cargaacademica idcarga, @Param("idpregunta") Pregunta idpregunta);

    @Query("SELECT r.pregunta.idpregunta, r.descripcion,SUM (r.ponderacion) FROM Respuesta r WHERE r.cargaacademica = :idcarga GROUP BY r.descripcion, r.pregunta")
    List<String> reportPdfEncuesta(@Param("idcarga") Cargaacademica idcarga);
}

//    SELECT SUM(r.ponderacion) as "ponderacion",r.descripcion, p.idpregunta as "Pregunta"
//        FROM bdencuesta.respuesta as r INNER JOIN bdencuesta.pregunta as p on r.idpregunta = p.idpregunta
//        WHERE idcargaacademica = 1
//        group by r.descripcion, r.idpregunta