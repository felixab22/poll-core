package com.sistemas.poll.dao;

import com.sistemas.poll.model.Encuesta;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by AITAMH on 8/01/2020.
 */
public interface IEncuestaDao extends JpaRepository<Encuesta, Long> {

}
