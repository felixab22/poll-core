package com.sistemas.poll.dao;

import com.sistemas.poll.model.Cargaacademica;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by AITAMH on 8/01/2020.
 */
public interface ICargaacademicaDao extends JpaRepository<Cargaacademica, Long> {

}
