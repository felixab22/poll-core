package com.sistemas.poll.dao;

import com.sistemas.poll.model.Encuesta;
import com.sistemas.poll.model.Pregunta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by AITAMH on 8/01/2020.
 */
public interface IPreguntaDao extends JpaRepository<Pregunta, Long> {

    List<Pregunta> getAllByEncuesta(Encuesta idencuesta);
}
