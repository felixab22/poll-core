package com.sistemas.poll.dao;

import com.sistemas.poll.model.Alumno;
import com.sistemas.poll.model.Encuestado;
import com.sistemas.poll.model.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IEncuestadoDao extends JpaRepository<Encuestado, Long> {

    Boolean existsByAlumnoAndEncuesta_Semestre(Alumno idalumno,String semestre);

    Encuestado findByAlumno(Alumno idalumno);

}
