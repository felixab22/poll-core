package com.sistemas.poll.dao;

import com.sistemas.poll.model.Alumno;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAlumnoDao extends JpaRepository<Alumno, Long> {

    Alumno findByCodigoAndDni(String codigo, String dni);

}
