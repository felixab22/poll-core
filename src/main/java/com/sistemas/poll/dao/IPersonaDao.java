package com.sistemas.poll.dao;

import com.sistemas.poll.model.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IPersonaDao extends JpaRepository<Persona,Long>{

    @Query("SELECT p FROM Persona p WHERE p.idpersona = :id")
    Persona getPersonaById(@Param("id") Persona id);

    Boolean existsByDni(int dni);
}
