package com.sistemas.poll.util;

import java.util.ArrayList;

public class pdfReportResponse {
    private ArrayList<Integer> pregunta;
    private ArrayList<String> calificacion;
    private ArrayList<Integer> ponderacion;

    public pdfReportResponse(ArrayList<Integer> pregunta, ArrayList<String> calificacion, ArrayList<Integer> ponderacion) {
        this.pregunta = pregunta;
        this.calificacion = calificacion;
        this.ponderacion = ponderacion;
    }

    public ArrayList<Integer> getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(ArrayList<Integer> ponderacion) {
        this.ponderacion = ponderacion;
    }

    public ArrayList<String> getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(ArrayList<String> calificacion) {
        this.calificacion = calificacion;
    }

    public ArrayList<Integer> getPregunta() {
        return pregunta;
    }

    public void setPregunta(ArrayList<Integer> pregunta) {
        this.pregunta = pregunta;
    }
}
