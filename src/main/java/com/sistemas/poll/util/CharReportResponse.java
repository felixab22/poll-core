package com.sistemas.poll.util;

import java.util.ArrayList;

/**
 * Created by AITAMH on 8/01/2020.
 */
public class CharReportResponse {

    private ArrayList<Integer> ponderacion;
    private ArrayList<String> calificacion;


    public CharReportResponse(ArrayList<Integer> ponderacion, ArrayList<String> calificacion) {
        this.ponderacion = ponderacion;
        this.calificacion = calificacion;
    }


    public ArrayList<Integer> getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(ArrayList<Integer> ponderacion) {
        this.ponderacion = ponderacion;
    }

    public ArrayList<String> getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(ArrayList<String> calificacion) {
        this.calificacion = calificacion;
    }

}
