package com.sistemas.poll.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by AITAMH on 7/01/2020.
 */
@Entity
@Table(name = "ENCUESTA")
public class Encuesta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDENCUESTA", length =11)
    private Long idencuesta;

    @Column(name = "DENOMINACION", length=200, nullable=false, unique=false)
    private String denominacion;

    @Column(name = "FINICIO", nullable = false)
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finicio;

    @Column(name = "FFIN", nullable = false)
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date ffin;

    @Column(name = "FCREACION", nullable = false)
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fcreacion;

    @Column(name = "SEMESTRE", length = 20, nullable = false)
    private String semestre;

    @OneToMany(mappedBy = "encuesta"   , cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Pregunta> preguntas = new ArrayList<>();

    public Encuesta() {
    }

    public Encuesta(Long idencuesta) {
        this.idencuesta = idencuesta;
    }

    public Encuesta(String denominacion, Date finicio, Date ffin, Date fcreacion, String semestre) {
        this.denominacion = denominacion;
        this.finicio = finicio;
        this.ffin = ffin;
        this.fcreacion = fcreacion;
        this.semestre = semestre;
    }

    public Long getIdencuesta() {
        return idencuesta;
    }

    public void setIdencuesta(Long idencuesta) {
        this.idencuesta = idencuesta;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    public Date getFinicio() {
        return finicio;
    }

    public void setFinicio(Date finicio) {
        this.finicio = finicio;
    }

    public Date getFfin() {
        return ffin;
    }

    public void setFfin(Date ffin) {
        this.ffin = ffin;
    }

    public Date getFcreacion() {
        return fcreacion;
    }

    public void setFcreacion(Date fcreacion) {
        this.fcreacion = fcreacion;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public List<Pregunta> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }
}
