package com.sistemas.poll.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by AITAMH on 7/01/2020.
 */

@Entity
@Table(name = "PREGUNTA")
public class Pregunta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDPREGUNTA", length =11)
    private Long idpregunta;

    @Column(name = "ENUNCIADO", nullable = false, length = 255)
    private String enunciado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDENCUESTA")
    @JsonBackReference
    private Encuesta encuesta;

    public Pregunta() {
    }

    public Pregunta(Long idpregunta) {
        this.idpregunta = idpregunta;
    }

    public Pregunta(String enunciado, Encuesta encuesta) {
        this.enunciado = enunciado;
        this.encuesta = encuesta;
    }

    public Long getIdpregunta() {
        return idpregunta;
    }

    public void setIdpregunta(Long idpregunta) {
        this.idpregunta = idpregunta;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public Encuesta getEncuesta() {
        return encuesta;
    }

    public void setEncuesta(Encuesta encuesta) {
        this.encuesta = encuesta;
    }
}
