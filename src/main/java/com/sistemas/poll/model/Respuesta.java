package com.sistemas.poll.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by AITAMH on 7/01/2020.
 */

@Entity
@Table(name = "RESPUESTA")
public class Respuesta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDRESPUESTA", length =11)
    private Long idrespuesta;

    @Column(name = "DESCRIPCION", length = 20, nullable = false)
    private String descripcion;

    @Column(name = "PONDERACION", length = 11, nullable = false)
    private int ponderacion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDCARGAACADEMICA")
    private Cargaacademica cargaacademica;

//    @ManyToOne(fetch = FetchType.LAZY)
    @ManyToOne
    @JoinColumn(name = "IDPREGUNTA")
    private Pregunta pregunta;

    public Respuesta(Long idrespuesta) {
        this.idrespuesta = idrespuesta;
    }

    public Respuesta() {
    }

    public Respuesta(String descripcion, int ponderacion, Cargaacademica cargaacademica, Pregunta pregunta) {
        this.descripcion = descripcion;
        this.ponderacion = ponderacion;
        this.cargaacademica = cargaacademica;
        this.pregunta = pregunta;
    }

    public Long getIdrespuesta() {
        return idrespuesta;
    }

    public void setIdrespuesta(Long idrespuesta) {
        this.idrespuesta = idrespuesta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(int ponderacion) {
        this.ponderacion = ponderacion;
    }

    public Cargaacademica getCargaacademica() {
        return cargaacademica;
    }

    public void setCargaacademica(Cargaacademica cargaacademica) {
        this.cargaacademica = cargaacademica;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }
}
