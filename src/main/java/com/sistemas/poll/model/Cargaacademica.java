package com.sistemas.poll.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by AITAMH on 7/01/2020.
 */

@Entity
@Table(name = "CARGAACADEMICA")
public class Cargaacademica implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDCARGA", length =11)
    private Long idcarga;

    @Column(name = "NOMBRESDOCENTE", length=100, nullable=false, unique=false)
    private String nombredocente;

    //@Column(name = "CODIGODOCENTE", length = 11, nullable = false)
    //private String codigodocente;

    //@Column(name = "SIGLA", length = 20, nullable = false)
    //private String sigla;

    @Column(name = "ASIGNATURA", length = 100, nullable = false)
    private String asignatura;

    @Column(name = "GRUPO", length = 11, nullable = true)
    private String grupo;

    public Cargaacademica() {
    }

    public Cargaacademica(Long idcarga) {
        this.idcarga = idcarga;
    }

    public Cargaacademica(String nombredocente, String asignatura, String grupo) {
        this.nombredocente = nombredocente;
        this.asignatura = asignatura;
        this.grupo = grupo;
    }

    public Long getIdcarga() {
        return idcarga;
    }

    public void setIdcarga(Long idcarga) {
        this.idcarga = idcarga;
    }

    public String getNombredocente() {
        return nombredocente;
    }

    public void setNombredocente(String nombredocente) {
        this.nombredocente = nombredocente;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }
}
