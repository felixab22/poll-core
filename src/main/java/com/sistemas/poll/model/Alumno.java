package com.sistemas.poll.model;

import javax.persistence.*;
import java.io.Serializable;

 @Entity
 @Table(name = "ALUMNO")
public class Alumno implements Serializable {

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "IDALUMNO", length =11)
     private Long idalumno;

     @Column(name = "SERIE")
     private String serie;

     @Column(name = "ALUMNO")
     private String alumno;

     @Column(name = "CREDITOS")
     private String creditos;

     @Column(name = "DIRECCION")
     private String direccion;

     @Column(name = "CODIGO")
     private String codigo;

     @Column(name = "DNI")
     private String dni;

     @Column(name = "ESCUELA")
     private String escuela;

     @Column(name = "FACULTAD")
     private String facultad;

     public Alumno() {
     }

     public Alumno(Long idalumno) {
         this.idalumno = idalumno;
     }

     public Alumno(String serie, String alumno, String creditos, String direccion, String codigo, String dni, String escuela, String facultad) {
         this.serie = serie;
         this.alumno = alumno;
         this.creditos = creditos;
         this.direccion = direccion;
         this.codigo = codigo;
         this.dni = dni;
         this.escuela = escuela;
         this.facultad = facultad;
     }

     public Long getIdalumno() {
         return idalumno;
     }

     public void setIdalumno(Long idalumno) {
         this.idalumno = idalumno;
     }

     public String getSerie() {
         return serie;
     }

     public void setSerie(String serie) {
         this.serie = serie;
     }

     public String getAlumno() {
         return alumno;
     }

     public void setAlumno(String alumno) {
         this.alumno = alumno;
     }

     public String getCreditos() {
         return creditos;
     }

     public void setCreditos(String creditos) {
         this.creditos = creditos;
     }

     public String getDireccion() {
         return direccion;
     }

     public void setDireccion(String direccion) {
         this.direccion = direccion;
     }

     public String getCodigo() {
         return codigo;
     }

     public void setCodigo(String codigo) {
         this.codigo = codigo;
     }

     public String getDni() {
         return dni;
     }

     public void setDni(String dni) {
         this.dni = dni;
     }

     public String getEscuela() {
         return escuela;
     }

     public void setEscuela(String escuela) {
         this.escuela = escuela;
     }

     public String getFacultad() {
         return facultad;
     }

     public void setFacultad(String facultad) {
         this.facultad = facultad;
     }
 }
