package com.sistemas.poll.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "encuestado")
public class Encuestado implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDENCUESTADO", length =11)
    private Long idencuestado;

    @Column(name = "FENCUESTA")
    private Date fencuesta;

    @Column(name = "ESTADO")
    private Boolean estado;

    @Column(name = "NUMENCUESTADO",length =11)
    private int numencuestado;

    @OneToOne
    @JoinColumn(name = "IDENCUESTA",  nullable = false)
    private Encuesta encuesta;

    @ManyToOne
    @JoinColumn(name = "IDALUMNO", nullable = false)
    private Alumno alumno;

    public Encuestado() {
    }

    public Encuestado(Long idencuestado) {
        this.idencuestado = idencuestado;
    }

    public Encuestado(Date fencuesta, Boolean estado, int numencuestado, Encuesta encuesta, Alumno alumno) {
        this.fencuesta = fencuesta;
        this.estado = estado;
        this.numencuestado = numencuestado;
        this.encuesta = encuesta;
        this.alumno = alumno;
    }

    public Long getIdencuestado() {
        return idencuestado;
    }

    public void setIdencuestado(Long idencuestado) {
        this.idencuestado = idencuestado;
    }

    public Date getFencuesta() {
        return fencuesta;
    }

    public void setFencuesta(Date fencuesta) {
        this.fencuesta = fencuesta;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public int getNumencuestado() {
        return numencuestado;
    }

    public void setNumencuestado(int numencuestado) {
        this.numencuestado = numencuestado;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Encuesta getEncuesta() {
        return encuesta;
    }

    public void setEncuesta(Encuesta encuesta) {
        this.encuesta = encuesta;
    }
}
