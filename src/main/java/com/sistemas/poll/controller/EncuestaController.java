package com.sistemas.poll.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sistemas.poll.model.Encuesta;
import com.sistemas.poll.model.Encuestado;
import com.sistemas.poll.service.IEncuestaService;
import com.sistemas.poll.service.IEncuestadoService;
import com.sistemas.poll.util.RestResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/encuesta")
public class EncuestaController {

    @Autowired
    private IEncuestaService encuestaService;

    @Autowired
    private IEncuestadoService encuestadoService;

    @Autowired
    private ObjectMapper objectMapper;

    private static final Log LOGGER = LogFactory.getLog(EncuestaController.class);

    @PostMapping("/saveEncuesta")
    public RestResponse saveEncuesta(@RequestBody String encuestaJson) throws JsonProcessingException {
       this.objectMapper = new ObjectMapper();
        LOGGER.info(" PARAMETROS DE ENTRADA: '" + encuestaJson + "'");
        Encuesta encuesta = objectMapper.readValue(encuestaJson, Encuesta.class);

        if (!validate(encuesta)){
            return  new RestResponse(HttpStatus.BAD_REQUEST.value(),"Campos obligatorios no diligenciados");
        }
        encuestaService.saveEncuesta(encuesta);
        return new RestResponse(HttpStatus.OK.value(),"Guardado correctamente",encuesta);
    }

    @GetMapping("/getAllEncuesta")
    public RestResponse getAllEncuesta(){
        List<Encuesta> result = this.encuestaService.getAllEncuesta();

        if (result.isEmpty()){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Aun no se ha realizaod encuesta alguna");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registro encontrado", result);
    }

    @PostMapping("/saveEncuestado")
    public RestResponse saveEncuestado(@RequestBody String encuestadoJson) throws JsonProcessingException {

        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMETROS DE ENTRADA: '" + encuestadoJson + "'");

        Encuestado encuestado = objectMapper.readValue(encuestadoJson, Encuestado.class);

        Boolean result = this.encuestadoService.getEncuestado(encuestado.getAlumno(), encuestado.getEncuesta().getSemestre());

        if (!result){
            this.encuestadoService.saveEncuestado(encuestado);
            return  new RestResponse(HttpStatus.OK.value(),"Guardado correctamente",encuestado);
        }
        return new RestResponse(HttpStatus.EXPECTATION_FAILED.value(),"El alumno ya realizo una encuesta, no puede realizar otra encuesta");
    }

    private boolean validate(Encuesta encuesta) {
        boolean isValid = true;

        if (StringUtils.trimToNull(encuesta.getDenominacion()) == null) {
            isValid = false;
        }
        if (StringUtils.trimToNull(String.valueOf(encuesta.getFcreacion())) == null) {
            isValid = false;
        }
        if (StringUtils.trimToNull(String.valueOf(encuesta.getFfin())) == null) {
            isValid = false;
        }
        if (StringUtils.trimToNull(String.valueOf(encuesta.getFinicio())) == null) {
            isValid = false;
        }
        if (StringUtils.trimToNull(encuesta.getSemestre()) == null) {
            isValid = false;
        }
        return isValid;
    }
}
