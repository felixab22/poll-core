package com.sistemas.poll.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sistemas.poll.model.Encuesta;
import com.sistemas.poll.model.Pregunta;
import com.sistemas.poll.service.IPreguntaService;
import com.sistemas.poll.util.RestResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pregunta")
public class PreguntaController {

    @Autowired
    private IPreguntaService preguntaService;

    @Autowired
    private ObjectMapper objectMapper;

    private static final Log LOGGER = LogFactory.getLog(PreguntaController.class);

    @PostMapping("/savePregunta")
    public RestResponse saveEncuesta(@RequestBody String preguntaJson) throws JsonProcessingException {

        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMETROS DE ENTRADA: '" + preguntaJson + "'");

        Pregunta pregunta = objectMapper.readValue(preguntaJson, Pregunta.class);

        if (!validate(pregunta)){
            return  new RestResponse(HttpStatus.BAD_REQUEST.value(),"Campos obligatorios no diligenciados");
        }
        this.preguntaService.savePregunta(pregunta);
        return new RestResponse(HttpStatus.OK.value(),"Guardado correctamente",pregunta);
    }

    @GetMapping("/getAllPreguntaByEncuesta")
    public RestResponse getAllPreguntaByEncuesta(@RequestParam Encuesta idencuesta){
        List<Pregunta> result = this.preguntaService.getPreguntasBySemestre(idencuesta);

        if (result.isEmpty()){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Aun no se ha realizaod encuesta alguna");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registro encontrado", result);
    }

    private boolean validate(Pregunta pregunta) {
        boolean isValid = true;

        if (StringUtils.trimToNull(pregunta.getEnunciado()) == null) {
            isValid = false;
        }
        if (StringUtils.trimToNull(String.valueOf(pregunta.getEncuesta())) == null) {
            isValid = false;
        }

        return isValid;
    }
}
