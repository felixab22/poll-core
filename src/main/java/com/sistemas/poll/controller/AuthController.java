package com.sistemas.poll.controller;

import com.sistemas.poll.dao.IRoleDao;
import com.sistemas.poll.dao.IUsuarioDao;
import com.sistemas.poll.model.Role;
import com.sistemas.poll.model.User;
import com.sistemas.poll.payload.JwtAuthenticationResponse;
import com.sistemas.poll.payload.LoginRequest;
import com.sistemas.poll.payload.SignUpRequest;
import com.sistemas.poll.security.JwtTokenProvider;
import com.sistemas.poll.service.IPersonaService;
import com.sistemas.poll.service.IUserService;
import com.sistemas.poll.util.ResponseMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by HeverFernandez on 02/08/17.
 */
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUsuarioDao usuarioDao;

    @Autowired
    private IRoleDao roleDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private IPersonaService personaService;

    @Autowired
    private IUserService userService;

    private static final Log LOGGER = LogFactory.getLog(AuthController.class);

    @PostMapping("/signin")
    public ResponseEntity<?>  authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );
//        LOGGER.info(" PARAMETROS DE LOGUEO: <<<<<<<>>>>>> '"+ loginRequest.getUsernameOrEmail() + "'");
//        LOGGER.info(" PARAMETROS DE LOGUEO: <<<<<<<>>>>>> '"+ loginRequest.getPassword() + "'");

        User user = this.userService.getPersonaByUsuario(loginRequest.getUsernameOrEmail());

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);

        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, userDetails.getUsername(),userDetails.getAuthorities(), user.getPersona()));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {

        LOGGER.info(" PARAMETROS DE LOGUEO: <<<<<<<>>>>>> '"+ signUpRequest.toString() + "'");

        if (usuarioDao.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        User user = new User( signUpRequest.getUsername(),passwordEncoder.encode(signUpRequest.getPassword()),signUpRequest.getIdpersona());

        Long strRoles = signUpRequest.getIdrole();

        Set<Role> roles = new HashSet<>();

                Role adminRole = this.roleDao.findByIdrol(strRoles);

                roles.add(adminRole);

        user.setRoles(roles);
        usuarioDao.save(user);

        return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
    }
}
