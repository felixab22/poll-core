package com.sistemas.poll.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sistemas.poll.dao.IRespuestaDao;
import com.sistemas.poll.model.Cargaacademica;
import com.sistemas.poll.model.Pregunta;
import com.sistemas.poll.model.Respuesta;
import com.sistemas.poll.service.IEncuestaService;
import com.sistemas.poll.service.IRespuestaService;
import com.sistemas.poll.util.CharReportResponse;
import com.sistemas.poll.util.RestResponse;
import com.sistemas.poll.util.pdfReportResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/respuesta")
public class RespuestaController {

    @Autowired
    private IRespuestaService respuestaService;

    @Autowired
    private IRespuestaDao respuestaDao;

    @Autowired
    private IEncuestaService encuestaService;

    @Autowired
    private ObjectMapper objectMapper;

    private static final Log LOGGER = LogFactory.getLog(EncuestaController.class);

    @PostMapping("/saveRespuesta")
    public RestResponse saveEncuesta(@RequestBody List<Respuesta> respuestasJson) throws JsonProcessingException {

        this.objectMapper = new ObjectMapper();
        //            System.out.println( "RESULTADOS : PREGUNTA : " + respuesta.getPregunta().getIdpregunta() + "  PONDERACION  : "+respuesta.getPonderacion() + " CALIF : "+respuesta.getDescripcion());
        List<Respuesta> result = new ArrayList<>(respuestasJson);

        if (result.isEmpty()){

            return new RestResponse(HttpStatus.EXPECTATION_FAILED.value(),"El alumno ya realizo una encuesta, no puede realizar otra encuesta");
        }
        this.respuestaDao.saveAll(result);

        return  new RestResponse(HttpStatus.OK.value(),"Guardado correctamente");
    }

       @GetMapping("/getReporteEncuestaByPreguntaAndDocente")
    public RestResponse getAllPreguntaByEncuesta(@RequestParam Cargaacademica idcarga, @RequestParam Pregunta idpregunta){

        CharReportResponse result = this.respuestaService.reporteEncuestaByPreguntaAndDocente(idcarga, idpregunta);

        if (result == null){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Aun no cuenta con evaluacion alguna");
        }
        return new RestResponse(HttpStatus.OK.value(),"La calificacion para el docente es :", result);
    }

    @GetMapping("/getReporteByDocente")
    public RestResponse getReporteByDocente(@RequestParam Cargaacademica idcarga){

        pdfReportResponse result = this.respuestaService.reporteEncuestaByDocente(idcarga);

        if (result == null){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Aun no cuenta con evaluacion alguna");
        }
        return new RestResponse(HttpStatus.OK.value(),"La calificacion para el docente es :", result);
    }

    private boolean validate(Respuesta respuesta) {
        boolean isValid = true;

        if (StringUtils.trimToNull(respuesta.getDescripcion()) == null) {
            isValid = false;
        }
        if (respuesta.getPonderacion() == 0) {
            isValid = false;
        }
        if (respuesta.getCargaacademica() == null) {
            isValid = false;
        }
        if (respuesta.getPregunta() == null) {
            isValid = false;
        }

        return isValid;
    }
}
