package com.sistemas.poll.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sistemas.poll.model.Cargaacademica;
import com.sistemas.poll.model.Encuesta;
import com.sistemas.poll.service.ICargaAcademicaService;
import com.sistemas.poll.util.RestResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by AITAMH on 9/01/2020.
 */
@RestController
@RequestMapping("/cargaacademica")
public class CargaAcademicaController {

    @Autowired
    private ICargaAcademicaService cargaAcademicaService;

    @Autowired
    private ObjectMapper objectMapper;

    private static final Log LOGGER = LogFactory.getLog(EncuestaController.class);

    @PostMapping("/saveCarga")
    public RestResponse saveCarga(@RequestBody String cargaJson) throws JsonProcessingException {
        this.objectMapper = new ObjectMapper();

        LOGGER.info(" PARAMETROS DE ENTRADA: '" + cargaJson + "'");

        Cargaacademica cargaacademica = objectMapper.readValue(cargaJson, Cargaacademica.class);

        if (!validate(cargaacademica)){

            return  new RestResponse(HttpStatus.BAD_REQUEST.value(),"Campos obligatorios no diligenciados");
        }
        this.cargaAcademicaService.saveCargaacademica(cargaacademica);

        return new RestResponse(HttpStatus.OK.value(),"Guardado correctamente",cargaacademica);
    }

    @GetMapping("/getAllCarga")
    public RestResponse getAllCarga(){

        List<Cargaacademica> result = this.cargaAcademicaService.getAllCargaAcademica();

        if (result.isEmpty()){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Aun no se tiene registro alguno");
        }
        return new RestResponse(HttpStatus.OK.value(),"Registro encontrado", result);
    }

    @PostMapping("/deleteCarga")
    public void deleteCarga(@RequestParam Long idcarga){
        this.cargaAcademicaService.deleteCargaacademica(idcarga);
    }

    private boolean validate(Cargaacademica carga) {
        boolean isValid = true;

        if (StringUtils.trimToNull(carga.getAsignatura()) == null) {
            isValid = false;
        }
        if (StringUtils.trimToNull(carga.getGrupo()) == null) {
            isValid = false;
        }
        if (StringUtils.trimToNull(carga.getNombredocente()) == null) {
            isValid = false;
        }

        return isValid;
    }
}
