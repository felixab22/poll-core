package com.sistemas.poll.controller;

import com.sistemas.poll.model.Alumno;
import com.sistemas.poll.model.Encuestado;
import com.sistemas.poll.payload.LoginRequest;
import com.sistemas.poll.service.IAlumnoService;
import com.sistemas.poll.service.IEncuestadoService;
import com.sistemas.poll.util.RestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/alumno")
public class AlumnoController {

    @Autowired
    private IAlumnoService alumnoService;

    @Autowired
    private IEncuestadoService encuestadoService;

    private static final Log LOGGER = LogFactory.getLog(EncuestaController.class);

    @PostMapping("/authAlumno")
    public RestResponse authAlumno(@Valid @RequestBody LoginRequest loginRequest){

//        LOGGER.info(" PARAMETROS DE ENTRADA: '" + loginRequest.getUsernameOrEmail() + " PASSWORD " + loginRequest.getPassword());

        Alumno alumno = this.alumnoService.getAlumno(loginRequest.getUsernameOrEmail(), loginRequest.getPassword());

        Encuestado encuestado = this.encuestadoService.getEncuestadoAlumno(alumno);

        if (alumno == null){
            return new RestResponse(HttpStatus.NO_CONTENT.value(),"Alumno no se encuentra registrado");
        }else {
            if (encuestado == null){
                return new RestResponse(HttpStatus.OK.value(), "El alumno aun no ha realizado encuesta alguna", alumno);
            }
                return new RestResponse(HttpStatus.CREATED.value(), "El alumno ha realizado una encuesta el : "+encuestado.getFencuesta(), alumno);
        }
    }
}
