package com.sistemas.poll.service;

import com.sistemas.poll.model.Role;

import java.util.List;

public interface RolService {
    /**
     * Guarda un Rol
     *
     * @param rol
     * @return el rol guardado
     */
    Role save(Role rol);

    /**
     * Recupera la lista de roles
     *
     * @return lista de roles
     */
    List<Role> findAll();

    /**
     * Elimina un rol con el id recibido
     *
     * @param id
     */
    void deleteRol(Long id);
}
