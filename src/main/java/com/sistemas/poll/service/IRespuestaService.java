package com.sistemas.poll.service;

import com.sistemas.poll.dto.ReporteEncuestaDto;
import com.sistemas.poll.model.Cargaacademica;
import com.sistemas.poll.model.Pregunta;
import com.sistemas.poll.model.Respuesta;
import com.sistemas.poll.util.CharReportResponse;
import com.sistemas.poll.util.pdfReportResponse;

import java.util.List;

public interface IRespuestaService {

    /**
     * Guarda y actualiza una respuesta
     * @param respuesta
     * @return
     */
    Respuesta saveRespuesta(Respuesta respuesta);

    /**
     * Retorna el reporte consolidado de la encuesta por docente y por pregunta
     * @param idcarga
     * @param idpregunta
     * @return
     */
    CharReportResponse reporteEncuestaByPreguntaAndDocente(Cargaacademica idcarga, Pregunta idpregunta);

    /**
     * Reporta una la encuesta de un docente con todas las preguntas de una encuesta
     * @param idcarga
     * @return
     */
    pdfReportResponse reporteEncuestaByDocente(Cargaacademica idcarga);

}
