package com.sistemas.poll.service;

import com.sistemas.poll.model.Cargaacademica;

import java.util.List;

/**
 * Created by AITAMH on 9/01/2020.
 */
public interface ICargaAcademicaService {

    Cargaacademica saveCargaacademica(Cargaacademica cargaacademica);

    /**
     * Lista la carga academica de un semestre dado
     * @param
     * @return
     */
    List<Cargaacademica> getAllCargaAcademica();

    /**
     * Elimina un registro de la carga academica
     * @param idacarga
     * @return
     */
    public void deleteCargaacademica(Long idacarga);
}
