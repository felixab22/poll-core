package com.sistemas.poll.service;

import com.sistemas.poll.model.Alumno;

public interface IAlumnoService {

    /**
     * Retorna un alumno a travez de su codigo y dni
     * @param codigo
     * @param dni
     * @return
     */
    Alumno getAlumno(String codigo, String dni);
}
