package com.sistemas.poll.service;

import com.sistemas.poll.model.Persona;

import java.util.List;

public interface IPersonaService {

    Persona savePersona(Persona persona);

    List<Persona> getAllPerson();
}
