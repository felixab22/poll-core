package com.sistemas.poll.service;

import com.sistemas.poll.model.Encuesta;
import com.sistemas.poll.model.Pregunta;

import java.util.List;

public interface IPreguntaService {

    /**
     * Registra y actualiza una pregunta
     * @param pregunta
     * @return
     */
    Pregunta savePregunta(Pregunta pregunta);

    /**
     * Obtiene las preguntas de una encuesta
     * @param idencuesta
     * @return
     */
    List<Pregunta> getPreguntasBySemestre(Encuesta idencuesta);
}
