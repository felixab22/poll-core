package com.sistemas.poll.service;

import com.sistemas.poll.model.Alumno;
import com.sistemas.poll.model.Encuesta;
import com.sistemas.poll.model.Encuestado;

import java.util.List;

public interface IEncuestaService {

    /**
     * Guarda y actualiza una encuesta
     * @param encuesta
     * @return
     */
    Encuesta saveEncuesta(Encuesta encuesta);

    /**
     * Retorna todas las encuestas realizadas
     * @return
     */
    List<Encuesta> getAllEncuesta();

}
