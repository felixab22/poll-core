package com.sistemas.poll.service.impl;

import com.sistemas.poll.dao.IPersonaDao;
import com.sistemas.poll.model.Persona;
import com.sistemas.poll.service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaServiceImpl implements IPersonaService {

    @Autowired
    private IPersonaDao personaDao;

    @Override
    public Persona savePersona(Persona persona) {
        return this.personaDao.save(persona);
    }

    @Override
    public List<Persona> getAllPerson() {
        return this.personaDao.findAll();
    }
}
