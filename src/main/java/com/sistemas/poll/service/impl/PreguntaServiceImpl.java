package com.sistemas.poll.service.impl;

import com.sistemas.poll.dao.IPreguntaDao;
import com.sistemas.poll.model.Encuesta;
import com.sistemas.poll.model.Pregunta;
import com.sistemas.poll.service.IPreguntaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PreguntaServiceImpl implements IPreguntaService {

    @Autowired
    private IPreguntaDao preguntaDao;

    @Override
    public Pregunta savePregunta(Pregunta pregunta) {
        return this.preguntaDao.save(pregunta);
    }

    @Override
    public List<Pregunta> getPreguntasBySemestre(Encuesta idencuesta) {
        return this.preguntaDao.getAllByEncuesta(idencuesta);
    }
}
