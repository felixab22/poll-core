package com.sistemas.poll.service.impl;

import com.sistemas.poll.dao.IEncuestaDao;
import com.sistemas.poll.dao.IEncuestadoDao;
import com.sistemas.poll.model.Alumno;
import com.sistemas.poll.model.Encuesta;
import com.sistemas.poll.model.Encuestado;
import com.sistemas.poll.service.IEncuestaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EncuestaServiceImpl implements IEncuestaService {

    @Autowired
    private IEncuestaDao encuestaDao;

    @Override
    public Encuesta saveEncuesta(Encuesta encuesta) {
        return this.encuestaDao.save(encuesta);
    }

    @Override
    public List<Encuesta> getAllEncuesta() {
        return this.encuestaDao.findAll();
    }


}
