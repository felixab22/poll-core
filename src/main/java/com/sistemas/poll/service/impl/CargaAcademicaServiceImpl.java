package com.sistemas.poll.service.impl;

import com.sistemas.poll.dao.ICargaacademicaDao;
import com.sistemas.poll.model.Cargaacademica;
import com.sistemas.poll.service.ICargaAcademicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by AITAMH on 9/01/2020.
 */
@Service
public class CargaAcademicaServiceImpl implements ICargaAcademicaService {

    @Autowired
    private ICargaacademicaDao cargaacademicaDao;

    @Override
    public Cargaacademica saveCargaacademica(Cargaacademica cargaacademica) {
        return this.cargaacademicaDao.save(cargaacademica);
    }

    @Override
    public List<Cargaacademica> getAllCargaAcademica() {
        return this.cargaacademicaDao.findAll();
    }

    @Override
     public void deleteCargaacademica(Long idacarga) {
        this.cargaacademicaDao.deleteById(idacarga);
    }
}
