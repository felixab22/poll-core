package com.sistemas.poll.service.impl;

import com.sistemas.poll.dao.IEncuestadoDao;
import com.sistemas.poll.model.Alumno;
import com.sistemas.poll.model.Encuestado;
import com.sistemas.poll.service.IEncuestadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EncuestadoServiceImpl implements IEncuestadoService {

    @Autowired
    private IEncuestadoDao encuestadoDao;

    @Override
    public Encuestado saveEncuestado(Encuestado encuestado) {
        return this.encuestadoDao.save(encuestado);
    }

    @Override
    public Boolean getEncuestado(Alumno idalumno, String smestre) {
        return this.encuestadoDao.existsByAlumnoAndEncuesta_Semestre(idalumno, smestre);
    }

    @Override
    public Encuestado getEncuestadoAlumno(Alumno idalumno) {
        return this.encuestadoDao.findByAlumno(idalumno);
    }
}
