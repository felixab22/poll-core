package com.sistemas.poll.service.impl;

import com.sistemas.poll.dao.IRoleDao;
import com.sistemas.poll.model.Role;
import com.sistemas.poll.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolServiceImpl implements RolService {

    @Autowired
    protected IRoleDao roleDao;

    @Override
    public Role save(Role rol) {
        return this.roleDao.save(rol);
    }

    @Override
    public List<Role> findAll() {
        return this.roleDao.findAll();
    }

    @Override
    public void deleteRol(Long id) {
        this.roleDao.deleteById(id);
    }
}
