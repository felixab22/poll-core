package com.sistemas.poll.service.impl;

import com.sistemas.poll.dao.IPersonaDao;
import com.sistemas.poll.dao.IUsuarioDao;
import com.sistemas.poll.model.User;
import com.sistemas.poll.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private IUsuarioDao usuarioDao;

    @Autowired
    private IPersonaDao personaDao;

    @Override
    public User getPersonaByUsuario(String username) {
        System.out.println("ESTOY EN EL METODO SERVICE USER >>> ");
        User user = this.usuarioDao.findPersonaByUsername(username);

        System.out.println("ID EXTRAIDO DEL USUARIO >>>> " + user.getPersona());

        return user;
    }
}
