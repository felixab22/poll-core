package com.sistemas.poll.service.impl;

import com.sistemas.poll.dao.IAlumnoDao;
import com.sistemas.poll.model.Alumno;
import com.sistemas.poll.service.IAlumnoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AlumnoServiceImpl implements IAlumnoService {

    @Autowired
    private IAlumnoDao alumnoDao;

    @Override
    public Alumno getAlumno(String codigo, String dni) {
        return this.alumnoDao.findByCodigoAndDni(codigo,dni) ;
    }
}
