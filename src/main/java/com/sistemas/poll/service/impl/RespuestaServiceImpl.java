package com.sistemas.poll.service.impl;

import com.sistemas.poll.dao.IRespuestaDao;
import com.sistemas.poll.model.Cargaacademica;
import com.sistemas.poll.model.Pregunta;
import com.sistemas.poll.model.Respuesta;
import com.sistemas.poll.service.IRespuestaService;
import com.sistemas.poll.util.CharReportResponse;
import com.sistemas.poll.util.pdfReportResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RespuestaServiceImpl implements IRespuestaService {

    @Autowired
    private IRespuestaDao respuestaDao;

    @Override
    public Respuesta saveRespuesta(Respuesta respuesta) {
        return this.respuestaDao.save(respuesta);

    }

    @Override
    public CharReportResponse reporteEncuestaByPreguntaAndDocente(Cargaacademica idcarga, Pregunta idpregunta) {

        List<String> result = this.respuestaDao.reportConsolidadoEncuestaByPregunta(idcarga, idpregunta);

        ArrayList arrayPonderacio = new ArrayList();

        ArrayList arrayCalificacion = new ArrayList();

        for (String objects : result) {

            String[] parts = objects.split(",");
            Integer part1 = Integer.valueOf(parts[0]);
            String part2 = parts[1];
//            System.out.println("parte1 :" + part1 + " Parte2 : " + part2);

            arrayPonderacio.add(part1);

            arrayCalificacion.add(part2);

//            System.out.println( " TAMAÑO DE ARRAYS : " + arrayPonderacio.size() + " :: "+ arrayCalificacion.size());
//        System.out.println("PONDERACION  : " + objects);

        }
        return new CharReportResponse(arrayPonderacio,arrayCalificacion);
    }

    @Override
    public pdfReportResponse reporteEncuestaByDocente(Cargaacademica idcarga) {

        List<String> list = this.respuestaDao.reportPdfEncuesta(idcarga);

        ArrayList arrayPregunta = new ArrayList();
        ArrayList arrayCalificacion = new ArrayList();
        ArrayList arrayPonderacio = new ArrayList();

        for (String string : list){
//            System.out.println("RESULT STRING : " + string);
            String[] parts = string.split(",");
            Integer preg = Integer.valueOf(parts[0]);
            String calif = parts[1];
            Integer pond = Integer.valueOf(parts[2]);

            arrayPregunta.add(preg);
            arrayCalificacion.add(calif);
            arrayPonderacio.add(pond);
        }
        return new pdfReportResponse(arrayPregunta, arrayCalificacion, arrayPonderacio);
    }
}
