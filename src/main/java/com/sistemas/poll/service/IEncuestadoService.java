package com.sistemas.poll.service;

import com.sistemas.poll.model.Alumno;
import com.sistemas.poll.model.Encuestado;

public interface IEncuestadoService {

    /**
     * Guarda y actualiza los encuestados
     * @param encuestado
     * @return
     */
    Encuestado saveEncuestado(Encuestado encuestado);

    /**
     * Retorna si un alumno ha realizado ya la encuesta en un semestre academico
     * @param idalumno
     * @return
     */
    Boolean getEncuestado(Alumno idalumno, String semestre);

    /**
     * Retorna un alumno encuestado.
     * @param idalumno
     * @return
     */
    Encuestado getEncuestadoAlumno(Alumno idalumno);
}
