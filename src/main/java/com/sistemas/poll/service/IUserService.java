package com.sistemas.poll.service;

import com.sistemas.poll.model.User;

public interface IUserService {

    User getPersonaByUsuario(String username);
}
